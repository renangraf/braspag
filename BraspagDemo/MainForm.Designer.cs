﻿namespace BraspagDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCriar = new System.Windows.Forms.Button();
            this.btnCapturar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnEstornar = new System.Windows.Forms.Button();
            this.btnDesativarRecorrencia = new System.Windows.Forms.Button();
            this.btnReativarRecorrencia = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCriar
            // 
            this.btnCriar.Location = new System.Drawing.Point(16, 18);
            this.btnCriar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCriar.Name = "btnCriar";
            this.btnCriar.Size = new System.Drawing.Size(184, 28);
            this.btnCriar.TabIndex = 0;
            this.btnCriar.Text = "Criar Transação";
            this.btnCriar.UseVisualStyleBackColor = true;
            this.btnCriar.Click += new System.EventHandler(this.btnCriar_Click);
            // 
            // btnCapturar
            // 
            this.btnCapturar.Location = new System.Drawing.Point(16, 54);
            this.btnCapturar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCapturar.Name = "btnCapturar";
            this.btnCapturar.Size = new System.Drawing.Size(184, 28);
            this.btnCapturar.TabIndex = 1;
            this.btnCapturar.Text = "Capturar Transação";
            this.btnCapturar.UseVisualStyleBackColor = true;
            this.btnCapturar.Click += new System.EventHandler(this.btnCapturar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(16, 90);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(184, 28);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "Buscar Transação";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnEstornar
            // 
            this.btnEstornar.Location = new System.Drawing.Point(16, 126);
            this.btnEstornar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEstornar.Name = "btnEstornar";
            this.btnEstornar.Size = new System.Drawing.Size(184, 28);
            this.btnEstornar.TabIndex = 3;
            this.btnEstornar.Text = "Estornar Transação";
            this.btnEstornar.UseVisualStyleBackColor = true;
            this.btnEstornar.Click += new System.EventHandler(this.btnEstornar_Click);
            // 
            // btnDesativarRecorrencia
            // 
            this.btnDesativarRecorrencia.Location = new System.Drawing.Point(205, 54);
            this.btnDesativarRecorrencia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDesativarRecorrencia.Name = "btnDesativarRecorrencia";
            this.btnDesativarRecorrencia.Size = new System.Drawing.Size(184, 28);
            this.btnDesativarRecorrencia.TabIndex = 4;
            this.btnDesativarRecorrencia.Text = "Desativar Recorrência";
            this.btnDesativarRecorrencia.UseVisualStyleBackColor = true;
            this.btnDesativarRecorrencia.Click += new System.EventHandler(this.btnDesativarRecorrencia_Click);
            // 
            // btnReativarRecorrencia
            // 
            this.btnReativarRecorrencia.Location = new System.Drawing.Point(205, 90);
            this.btnReativarRecorrencia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReativarRecorrencia.Name = "btnReativarRecorrencia";
            this.btnReativarRecorrencia.Size = new System.Drawing.Size(184, 28);
            this.btnReativarRecorrencia.TabIndex = 5;
            this.btnReativarRecorrencia.Text = "Reativar Recorrência";
            this.btnReativarRecorrencia.UseVisualStyleBackColor = true;
            this.btnReativarRecorrencia.Click += new System.EventHandler(this.btnReativarRecorrencia_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(205, 126);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 28);
            this.button1.TabIndex = 6;
            this.button1.Text = "Consulta Recorrência";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnConsultaRecorrencia_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 181);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnReativarRecorrencia);
            this.Controls.Add(this.btnDesativarRecorrencia);
            this.Controls.Add(this.btnEstornar);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btnCapturar);
            this.Controls.Add(this.btnCriar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCriar;
        private System.Windows.Forms.Button btnCapturar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnEstornar;
        private System.Windows.Forms.Button btnDesativarRecorrencia;
        private System.Windows.Forms.Button btnReativarRecorrencia;
        private System.Windows.Forms.Button button1;
    }
}

