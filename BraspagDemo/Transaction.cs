﻿using Braspag;
using Braspag.Client;
using Braspag.Model;
using System;

namespace BraspagDemo
{
    public class Transaction
    {
        const string MerchantId = "12e85e0d-4e07-461a-a1d1-8712426df78a";
        const string MerchantKey = "UZXETBICZTKLSFUNMQBYTNRVWWYNXEPFGVNDAOEU";
        readonly EnvironmentType EnvironmentType = EnvironmentType.Sandbox;

        public TransactionResponse GetTransaction()
        {
            string paymentId = "915B1DB0-F017-4266-B26E-1C34E8BC1583";

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            TransactionResponse result = trans.GetTransaction(paymentId);

            return result;
        }

        public TransactionResponse CreateTransaction()
        {
            TransactionRequest transReq = new TransactionRequest()
            {
                MerchantOrderId = "123456",
                Customer = new Customer()
                {
                    Name = "Joao das Neves",
                    //Birthdate = DateTime.Parse("1990-01-01")
                },
                Payment = new Payment()
                {
                    Amount = 2099,
                    //Authenticate = false,
                    Capture = true,
                    ////Country = "BRA",
                    //Currency = Currency.BRL,
                    Installments = 1,
                    Type = TypeCard.CreditCard,
                    SoftDescription = "Soft Teste",
                    //Interest = "",
                    Provider = Provider.Simulado,
                    //Recurrent = true,
                    CreditCard = new Card()
                    {
                        Brand = Brand.Visa,
                        CardNumber = "4024007197692931",
                        ExpirationDate = "10/2020",
                        Holder = "Joao das Neves",
                        SecurityCode = "262",
                        SaveCard = false
                    },
                    RecurrentPayment = new RecurrentPayment()
                    {
                        AuthorizeNow = true,
                        Interval = Interval.Monthly
                    }
                }
            };

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            TransactionResponse result = trans.CreateTransaction(transReq);

            return result;
        }

        public CaptureResponse CaptureTransaction()
        {
            string paymentId = "10117102821185500001";
            int Amount = 2099;

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            CaptureResponse result = trans.CaptureTransaction(paymentId, Amount);

            return result;
        }

        public RefundResponse RefundTransaction()
        {
            string paymentId = "D780900C-0693-476A-A913-BF22A42848AF";
            int amount = 16900;

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            RefundResponse result = trans.RefundTransaction(paymentId, amount);

            return result;
        }

        public bool DeactivateRecurrent()
        {
            string recurrentPaymentId = "ca77bbb8-e47c-458c-9817-97e043fdad90";
            
            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            return trans.DeactivateRecurrent(recurrentPaymentId);
        }

        public bool ReactivateRecurrent()
        {
            string recurrentPaymentId = "ca77bbb8-e47c-458c-9817-97e043fdad90";

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            return trans.ReactivateRecurrent(recurrentPaymentId);
        }

        public TransactionRecurrentResponse GetRecurrentPayment()
        {
            string recurrentPaymentId = "47F5BD81-DB18-40AF-8391-62EB4582A49C";

            Transactions trans = new Transactions(MerchantId, MerchantKey, EnvironmentType);

            TransactionRecurrentResponse result = trans.GetRecurrentPayment(recurrentPaymentId);

            return result;
        }
    }
}
