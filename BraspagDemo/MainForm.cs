﻿using System;
using System.Windows.Forms;

namespace BraspagDemo
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCriar_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.CreateTransaction();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapturar_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.CaptureTransaction();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.GetTransaction();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEstornar_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.RefundTransaction();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReativarRecorrencia_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.ReactivateRecurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDesativarRecorrencia_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.DeactivateRecurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultaRecorrencia_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction transacao = new Transaction();

                transacao.GetRecurrentPayment();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
