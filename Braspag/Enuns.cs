﻿namespace Braspag
{
    public enum EnvironmentType
    {
        Sandbox,
        Production
    }

    public enum Provider
    {
        Simulado,
        Cielo,
        Cielo30,
        Getnet,
        RedeCard,
        Rede,
        Rede2,
        GlobalPayments,
        Stone,
        Safra,
        FirstData,
        Sub1,
        Banorte,
        Credibanco,
        Transbank,
        RedeSitef,
        CieloSitef,
        SantanderSitef,
        DMCard,
        Credsystem
    }

    public enum TypeCard
    {
        CreditCard,
        DebitCard
    }

    public enum Currency
    {
        BRL,
        USD,
        MXN,
        COP,
        CLP,
        ARS,
        PEN,
        EUR,
        PYN,
        UYU,
        VEB,
        VEF,
        GBP
    }

    public enum Interest
    {
        ByMerchant,
        ByIssuer
    }

    public enum IdentityType
    {
        CPF,
        CNPJ
    }

    public enum Brand
    {
        Visa,
        Master,
        Amex,
        Elo,
        Aura,
        Jcb,
        Diners,
        Discover,
        Hipercard,
        Hiper,
        Cabal,
        Naranja,
        Nevada,
        Carnet,
        Credential
    }

    public enum Interval
    {
        Monthly = 1,
        Bimonthly = 2,
        Quarterly = 3,
        SemiAnnual = 6,
        Annual = 12
    }

    public enum StatusRecurrentPayment
    {
        Ativo = 1,
        Finalizado = 2,
        DesativadoLojista = 3,
        DesativadoNumTentativas = 4,
        DesativadoCartaoVencido = 5
    }
}
