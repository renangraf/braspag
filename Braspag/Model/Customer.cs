﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Braspag.Model
{
    public class Customer
    {
        public string Name { get; set; }
        public string Identity { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public IdentityType IdentityType { get; set; }
        public string Email { get; set; }
        public DateTime? Birthdate { get; set; }

        public Address Address { get; set; }
        public Address DeliveryAdrress { get; set; }
    }
}
