﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Braspag.Model
{
    public class RecurrentPayment
    {
        public bool AuthorizeNow { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Interval Interval { get; set; }
        public string RecurrentPaymentId { get; set; }
        public int ReasonCode { get; set; }
        public string ReasonMessage { get; set; }
        public DateTime? NextRecurrency { get; set; }
        public StatusRecurrentPayment Status { get; set; }
        public Link Link { get; set; }
    }
}
