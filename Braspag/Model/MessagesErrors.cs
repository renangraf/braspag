﻿namespace Braspag.Model
{
    public class MessageError
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
