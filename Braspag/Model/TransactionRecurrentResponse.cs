﻿namespace Braspag.Model
{
    public class TransactionRecurrentResponse
    {
        public Customer Customer { get; set; }
        public RecurrentPayment RecurrentPayment { get; set; }
    }
}
