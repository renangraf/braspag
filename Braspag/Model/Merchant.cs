﻿namespace Braspag.Model
{
    public class Merchant
    {
        public string Id { get; set; }
        public string TradeName { get; set; }
    }
}
