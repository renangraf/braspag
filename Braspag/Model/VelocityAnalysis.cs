﻿using System;

namespace Braspag.Model
{
    public class VelocityAnalysis
    {
        public Guid Id { get; set; }
        public string ResultMessage { get; set; }
        public decimal Score { get; set; }
    }
}
