﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Braspag.Model
{
    public class Payment
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Provider Provider { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TypeCard Type { get; set; }
        public int Amount { get; set; }
        public int ServiceTaxAmount { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Currency Currency { get; set; }
        public string Country { get; set; } //(tamanho 3 ex.: BRA)
        public int Installments { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Interest Interest { get; set; }
        public bool Capture { get; set; }
        public bool Authenticate { get; set; }
        public bool Recurrent { get; set; }
        public string SoftDescription { get; set; }
        public string ProofOfSale { get; set; }
        public string AcquirerTransactionId { get; set; }
        public string AuthorizationCode { get; set; }
        public Guid PaymentId { get; set; }
        public DateTime ReceivedDate { get; set; }
        public int CapturedAmount { get; set; }
        public DateTime? CapturedDate { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonMessage { get; set; }
        public int Status { get; set; }
        public string ProviderReturnCode { get; set; }
        public string ProviderReturnMessage { get; set; }
        public string SentOrderId { get; set; }
        public string ProviderDescription { get; set; }
        public int VoidedAmount { get; set; }
        public DateTime? VoidedDate { get; set; }
        public string Tid { get; set; }
        public bool IsQrCode { get; set; }
        public bool IsSplitted { get; set; }

        public Card CreditCard { get; set; }
        public Card DebitCard { get; set; }
        public VelocityAnalysis VelocityAnalysis { get; set; }
        public RecurrentPayment RecurrentPayment { get; set; }
        public List<Link> Links { get; set; }
        public List<ExtraData> ExtraDataCollection { get; set; }
    }
}
