﻿namespace Braspag.Model
{
    public class TransactionResponse
    {
        public string MerchantOrderId { get; set; }

        public Customer Customer { get; set; }
        public Merchant Merchant { get; set; }
        public Payment Payment { get; set; }
    }
}
