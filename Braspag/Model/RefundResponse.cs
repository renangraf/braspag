﻿using System.Collections.Generic;

namespace Braspag.Model
{
    public class RefundResponse
    {
        public int Status { get; set; }
        public int ReasonCode { get; set; }
        public string ReasonMessage { get; set; }
        public string ProviderReturnCode { get; set; }
        public string ProviderReturnMessage { get; set; }
        public List<Link> Links { get; set; }
    }
}
