﻿namespace Braspag.Model
{
    public class TransactionRequest
    {
        public string MerchantOrderId { get; set; }

        public Customer Customer { get; set; }
        public Payment Payment { get; set; }
    }
}
