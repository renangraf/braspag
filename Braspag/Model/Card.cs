﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Braspag.Model
{
    public class Card
    {
        public string CardNumber { get; set; }
        public string Holder { get; set; }
        public string ExpirationDate { get; set; }  // ("MM/YYYY")
        public string SecurityCode { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Brand Brand { get; set; }
        public bool SaveCard { get; set; }
    }
}
