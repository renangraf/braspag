﻿using Braspag.Api;
using Braspag.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Braspag.Client
{
    public class Transactions
    {
        private readonly ApiClient ApiClient = null;
        private readonly string MerchantId = "";
        private readonly string MerchantKey = "";

        public Transactions(string merchantId, string merchantKey, EnvironmentType environmentType)
        {
            ApiClient = new ApiClient(environmentType);

            MerchantId = merchantId;
            MerchantKey = merchantKey;
        }

        /// <summary>
        /// Captura uma Transação 
        /// </summary>
        /// <param name="tid">IdTransação</param> 
        /// /// <param name="amount">Valor sem separador de milhar e decimal</param>
        /// <returns>TransactionResponse</returns>
        public CaptureResponse CaptureTransaction(string paymentId, int amount)
        {
            #region Validations
            // verify the required parameter 'tid' is set
            if (string.IsNullOrEmpty(paymentId))
                throw new ApiException(400, "Missing required parameter 'tid' when calling CaptureTransaction");

            // verify the required parameter 'amount' is set
            if (amount <= 0)
                throw new ApiException(400, "Missing required parameter 'amount' when calling CaptureTransaction");
            #endregion

            string path = $"/1/sales/{paymentId}/capture";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "{ \"amount\": " + amount.ToString() + " }"; // http body (model) parameter

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            CaptureResponse transResp = (CaptureResponse)ApiClient.Deserialize(response.Content, typeof(CaptureResponse), response.Headers);

            return transResp;
        }

        /// <summary>
        /// Cria uma Transação 
        /// </summary>
        /// <param name="transactionRequest">Transação</param>
        /// <returns>TransactionResponse</returns>
        public TransactionResponse CreateTransaction(TransactionRequest transaction)
        {
            string path = "1/sales/";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(transaction); // http body (model) parameter

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                    throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            TransactionResponse transResp = (TransactionResponse)ApiClient.Deserialize(response.Content, typeof(TransactionResponse), response.Headers);

            return transResp;
        }

        /// <summary>
        /// Operação Obtém detalhes de uma Transação 
        /// </summary>
        /// <param name="paymentId">id do pagamento</param> 
        /// <returns>TransactionResponse</returns>
        public TransactionResponse GetTransaction(string paymentId)
        {
            #region Validations
            // verify the required parameter 'id' is set
            if (string.IsNullOrEmpty(paymentId))
                throw new ApiException(400, "Missing required parameter 'tid' when calling GetTransaction");
            #endregion

            string path = $"/1/sales/{paymentId}";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "";

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            TransactionResponse transResp = (TransactionResponse)ApiClient.Deserialize(response.Content, typeof(TransactionResponse), response.Headers);

            return transResp;
        }

        /// <summary>
        /// Operação Obtém detalhes de uma Transação 
        /// </summary>
        /// <param name="redepayApiVersion">versão  da api</param> 
        /// <param name="id">id da Transação</param> 
        /// <param name="token">Token de acesso para quem esta efetuando a chamada aos recursos disponibilizados através da API. Tipo de parâmetro: header</param> 
        /// <returns>TransactionResponse</returns>
        public RefundResponse RefundTransaction(string paymentId, int amount)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(paymentId))
                throw new ApiException(400, "Missing required parameter 'tid' when calling RefundTransaction");

            // verify the required parameter 'Token' is set
            if (amount <= 0)
                throw new ApiException(400, "Missing required parameter 'amount' when calling RefundTransaction");
            #endregion

            string path = $"/1/sales/{paymentId}/void?amount={amount}";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "";

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            RefundResponse refundResp = (RefundResponse)ApiClient.Deserialize(response.Content, typeof(RefundResponse), response.Headers);

            return refundResp;
        }

        /// <summary>
        /// Cancela uma recorrência ativa
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <returns>True/False</returns>
        public bool DeactivateRecurrent(string recurrentPaymentId)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Deactivate";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "";

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Reativa uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <returns>True/False</returns>
        public bool ReactivateRecurrent(string recurrentPaymentId)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Reactivate";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "";

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica as informações do cliente de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="customer">Informações do cliente</param>
        /// <returns>True/False</returns>
        public bool ModifyCustomerRecurrent(string recurrentPaymentId, Customer customer)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (customer == null)
                throw new ApiException(400, "Customer not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Customer";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(customer);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica a data final de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="endDate">data final da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyEndDateRecurrent(string recurrentPaymentId, DateTime endDate)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (endDate < DateTime.Now)
                throw new ApiException(400, "dateEnd not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/EndDate";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(endDate);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica o intervalo de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="interval">intervalo da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyIntervalRecurrent(string recurrentPaymentId, Interval interval)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Interval";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(interval);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica o dia de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="day">dia da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyDayRecurrent(string recurrentPaymentId, int day)
        {
            #region Validations
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (day <= 0)
                throw new ApiException(400, "day is not valid!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/RecurrencyDay";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(day);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica o valor de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="amount">valor da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyAmountRecurrent(string recurrentPaymentId, int amount)
        {
            #region Validations
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (amount <= 0)
                throw new ApiException(400, "amount is not valid!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Amount";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(amount);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica a data de pagamento de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="nextPaymentDate">Próxima data de pagamento da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyNextPaymentDateRecurrent(string recurrentPaymentId, DateTime nextPaymentDate)
        {
            #region Validations
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (nextPaymentDate <= DateTime.Now)
                throw new ApiException(400, "nextPaymentDate is not valid!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/NextPaymentDate";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(nextPaymentDate);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Modifica as informações do pagamento de uma recorrência
        /// </summary>
        /// <param name="recurrentPaymentId">Id da recorrência</param>
        /// <param name="nextPaymentDate">Próxima data de pagamento da recorrência</param>
        /// <returns>True/False</returns>
        public bool ModifyPaymentDataRecurrent(string recurrentPaymentId, Payment payment)
        {
            #region Validations
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "RecurrentPaymentId not informed!");

            if (payment == null)
                throw new ApiException(400, "payment not informed!");
            #endregion

            var path = $"/1/RecurrentPayment/{recurrentPaymentId}/Payment";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = ApiClient.Serialize(payment);

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            return true;
        }

        /// <summary>
        /// Operação Obtém detalhes de uma Recorrencia 
        /// </summary>
        /// <param name="recurrentPaymentId">id da recorrencia</param> 
        /// <returns>TransactionResponse</returns>
        public TransactionRecurrentResponse GetRecurrentPayment(string recurrentPaymentId)
        {
            #region Validations
            // verify the required parameter 'recurrentPaymentId' is set
            if (string.IsNullOrEmpty(recurrentPaymentId))
                throw new ApiException(400, "Missing required parameter 'recurrentPaymentId' when calling GetRecurrentPayment");
            #endregion

            string path = $"/1/RecurrentPayment/{recurrentPaymentId}";

            Dictionary<string, string> queryParams = new Dictionary<string, string>();
            Dictionary<string, string> headerParams = GetHeaderDefault();

            string postBody = "";

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams);

            if (((int)response.StatusCode) >= 400)
            {
                List<MessageError> erros = (List<MessageError>)ApiClient.Deserialize(response.Content, typeof(List<MessageError>));

                if (erros != null)
                {
                    StringBuilder msgErro = new StringBuilder();
                    foreach (MessageError me in erros)
                        msgErro.AppendLine($"{me.Code} - {me.Message}");

                    throw new ApiException((int)response.StatusCode, msgErro.ToString());
                }
                else
                    throw new ApiException((int)response.StatusCode, $"{(int)response.StatusCode} - {response.StatusDescription}");
            }
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, response.ErrorMessage);

            TransactionRecurrentResponse transResp = (TransactionRecurrentResponse)ApiClient.Deserialize(response.Content, typeof(TransactionRecurrentResponse), response.Headers);

            return transResp;
        }

        private Dictionary<string, string> GetHeaderDefault()
        {
            return new Dictionary<string, string>
            {
                { "MerchantId", ApiClient.ParameterToString(this.MerchantId) }, // header parameter
                { "MerchantKey", ApiClient.ParameterToString(this.MerchantKey) } // header parameter
            };
        }
    }
}
